*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentEnp
${delLink}  /delEnp/18
*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}
    
กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 
*** Test Cases ***
TC-PSF-04-11-01ตรวจสอบการแสดงผลข้อมูลผู้ใช้ที่เลือกจากตาราง
    เปิด Browser    ${browser}
    กดลิ้งค์    ${delLink}
