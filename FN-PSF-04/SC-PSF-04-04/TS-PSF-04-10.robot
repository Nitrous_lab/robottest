*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentEnp
${delLink}  /delEnp/17
${btnCancle}  cancel
${btnOk}  ok
*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}
    
กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}
*** Test Cases ***
TC-PSF-04-10-01ตรวจสอบการกดปุ่มยกเลิก
    เปิด Browser    ${browser}
    กดลิ้งค์    ${delLink}
    กดปุ่ม    ${btnCancle}

TC-PSF-04-10-02ตรวจสอบการกดปุ่มตกลง
    เปิด Browser    ${browser}
    กดลิ้งค์    ${delLink}
    กดปุ่ม    ${btnOk}