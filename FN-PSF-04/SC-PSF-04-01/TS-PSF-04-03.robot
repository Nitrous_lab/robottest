*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentEnp
${searchfiled}  search
${search_input}  มีนา
${btnSearch}  btnsearch

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}

*** Test Cases ***
TC-PSF-04-03-01ตรวจสอบการแสดงผลข้อมูลรายชื่อนิสิตจบไม่ตรงแผน
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input}
    กดปุ่ม    ${btnSearch}

