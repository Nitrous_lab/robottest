*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentEnp
${btnSearch}  btnsearch
${btnAdd}  btnAdd
${editLink}  /manageEnp_edit/17
${delLink}  /delEnp/17


*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}
กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 

*** Test Cases ***
TC-PSF-04-02-01ตรวจสอบการกดปุ่มค้นหา
    เปิด Browser    ${browser}
    กดปุ่ม    ${btnSearch}

TC-PSF-04-02-02ตรวจสอบการกดปุ่มเพิ่มข้อมูล
    เปิด Browser    ${browser}
    กดปุ่ม    ${btnAdd}

TC-PSF-04-02-03ตรวจสอบการกดปุ่มแก้ไข
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}

TC-PSF-04-02-04ตรวจสอบการกดลบ
    เปิด Browser    ${browser}
    กดลิ้งค์    ${delLink}




