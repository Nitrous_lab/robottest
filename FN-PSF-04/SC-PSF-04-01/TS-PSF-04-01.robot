*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentEnp
${searchfiled}  search
${search_input}  มีนา
${search_input02}  มานี
${search_input03}  61160155
${search_input04}  22222222222222222222222222222222222222222222222222222
${btnSearch}  btnsearch

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}

*** Test Cases ***
TC-PSF-04-01-01ตรวจสอบการกรอกข้อมูลค้นหาจากชื่อ
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input}
    กดปุ่ม    ${btnSearch}

TC-PSF-04-01-02ตรวจสอบการกรอกข้อมูลค้นหาจากนามสกุล
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input02}
    กดปุ่ม    ${btnSearch}

TC-PSF-04-01-03ตรวจสอบการกรอกข้อมูลค้นหาจากรหัสนิสิต
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input03}
    กดปุ่ม    ${btnSearch}


TC-PSF-04-01-04ตรวจสอบการกรอกข้อมูลค้นหารับไม่เกิน 50ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input04}
    กดปุ่ม    ${btnSearch}


