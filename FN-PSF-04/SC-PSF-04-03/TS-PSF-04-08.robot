*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentEnp
${editLink}  /manageEnp_edit/17
${studentcodefiled}  studentIDEnp
${studentcode}  64160237
${namestudentfiled}  nameStuEnp
${namestudent}  จัตตรงณ์ เพิ่มศิลป์
${studentGpafiled}  stuGpaEnp
${studentGpa}  1.90
${creditPassfiled}  creditPassedEnp
${creditPass}  70
${creditDownfiled}  creditDownEnp
${creditDown}  95
${Label_Level}  stuLevelEnp
${Label_Status}  stuStatusEnp
${Lavel_value}  ตรี พิเศษ
${Status_value}  10
${btnOk}  ok
${btnCancle}  cancel


*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}

กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 

เลือกDropdown
    [Arguments]  ${label_name}  ${value}
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Select From List By Label    ${label_name}  ${value}
*** Test Cases ***
TC-PSF-04-08-01ตรวจสอบการกดปุ่มบันทึก
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${studentcodefiled}    ${studentcode}
    กรอกข้อมูล    ${namestudentfiled}    ${namestudent}
    เลือกDropdown    ${Label_Level}    ${Lavel_value}
    กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}
    เลือกDropdown    ${Label_Status}    ${Status_value}
    กรอกข้อมูล    ${creditPassfiled}    ${creditPass}
    กรอกข้อมูล    ${creditDownfiled}    ${creditDown}
    กดปุ่ม    ${btnOk}

TC-PSF-04-08-02ตรวจสอบการกดปุ่มยกเลิก
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${studentcodefiled}    ${studentcode}
    กรอกข้อมูล    ${namestudentfiled}    ${namestudent}
    เลือกDropdown    ${Label_Level}    ${Lavel_value}
    กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}
    เลือกDropdown    ${Label_Status}    ${Status_value}
    กรอกข้อมูล    ${creditPassfiled}    ${creditPass}
    กรอกข้อมูล    ${creditDownfiled}    ${creditDown}
    กดปุ่ม    ${btnCancle}