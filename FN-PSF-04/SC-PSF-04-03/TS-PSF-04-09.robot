*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentEnp
${editLink}  /manageEnp_edit/1
${Label_Level}  stuLevelEnp
${Label_Status}  stuStatusEnp
*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}
    
กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 
คลิกDropdown
    [Arguments]  ${label_name} 
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Click Element    ${label_name}
*** Test Cases ***
TC-PSF-04-09-01ตรวจสอบการแสดงผลของฟิลเตอร์ Dropdown ข้อมูลระดับ
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    คลิกDropdown    ${Label_Level}
TC-PSF-04-09-02ตรวจสอบการแสดงผลของฟิลเตอร์ Dropdown ข้อมูลสถานภาพ
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    คลิกDropdown    ${Label_Status}

TC-PSF-04-09-03ตรวจสอบการแสดงผลข้อมูลผู้ใช้ที่เลือกจากตาราง
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}