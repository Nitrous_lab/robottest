*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageEnp_edit/18
${editLink}  /manageEnp_edit/17
${studentcodefiled}  studentIDEnp
${studentcode}  64160237
${namestudentfiled}  nameStuEnp
${namestudent}  จัตตรงณ์ เพิ่มศิลป์
${studentGpafiled}  stuGpaEnp
${studentGpa}  1.90
${creditPassfiled}  creditPassedEnp
${creditPass}  70
${creditDownfiled}  creditDownEnp
${creditDown}  95
${studentcode06}  641602375
${namestudent07}  55555555555555555555555555555555555555555555555555
${studentGpa08}  1.90007
${creditPass09}  70000
${creditDown10}  95000
${Label_Level}  stuLevelEnp
${Label_Status}  stuStatusEnp
${Lavel_value}  ตรี พิเศษ
${Status_value}  10
${btnOk}  ok
${studentcode11}
${namestudent12}
${studentGpa13}
${creditPass14}
${creditDown15}
*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}

กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 

เลือกDropdown
    [Arguments]  ${label_name}  ${value}
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Select From List By Label    ${label_name}  ${value}

*** Test Cases ***
# TC-PSF-04-07-01ตรวจสอบการกรอกข้อมูลรหัสนิสิต
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${studentcodefiled}    ${studentcode}

# TC-PSF-04-07-02ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${namestudentfiled}    ${namestudent}

# TC-PSF-04-07-03ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ย
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}

# TC-PSF-04-07-04ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่าน
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${creditPassfiled}    ${creditPass}

# TC-PSF-04-07-05ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลง
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${creditDownfiled}    ${creditDown}

# TC-PSF-04-07-06ตรวจสอบการกรอกข้อมูลรหัสนิสิต รับไม่เกิน 8 ตัวอักษร และ รับไม่น้อยกว่า 8 ตัวอักษร
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${studentcodefiled}    ${studentcode06}

# TC-PSF-04-07-07ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล รับไม่เกิน 50 ตัวอักษร
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${namestudentfiled}    ${namestudent07}

# TC-PSF-04-07-08ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ยรับทศนิยมไม่เกิน 2 หลัก และรับไม่น้อยกว่า 2 หลัก
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${studentGpafiled}    ${studentGpa08}

# TC-PSF-04-07-09ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่านรับเลขไม่เกิน 3 หลัก
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${creditPassfiled}    ${creditPass09}

# TC-PSF-04-07-10ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลงรับเลขไม่เกิน 3 หลัก
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${creditDownfiled}    ${creditDown10}

# TC-PSF-04-07-11ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลรหัสนิสิต
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${studentcodefiled}    ${studentcode11}
#     กรอกข้อมูล    ${namestudentfiled}    ${namestudent}
#     เลือกDropdown    ${Label_Level}    ${Lavel_value}
#     กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}
#     เลือกDropdown    ${Label_Status}    ${Status_value}
#     กรอกข้อมูล    ${creditPassfiled}    ${creditPass}
#     กรอกข้อมูล    ${creditDownfiled}    ${creditDown}
#     กดปุ่ม    ${btnOk}


# TC-PSF-04-07-12ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลชื่อ-นามสกุล
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${studentcodefiled}    ${studentcode}
#     กรอกข้อมูล    ${namestudentfiled}    ${namestudent12}
#     เลือกDropdown    ${Label_Level}    ${Lavel_value}
#     กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}
#     เลือกDropdown    ${Label_Status}    ${Status_value}
#     กรอกข้อมูล    ${creditPassfiled}    ${creditPass}
#     กรอกข้อมูล    ${creditDownfiled}    ${creditDown}
#     กดปุ่ม    ${btnOk}



# TC-PSF-04-07-13ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลเกรดเฉลี่ย
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${studentcodefiled}    ${studentcode}
#     กรอกข้อมูล    ${namestudentfiled}    ${namestudent}
#     เลือกDropdown    ${Label_Level}    ${Lavel_value}
#     กรอกข้อมูล    ${studentGpafiled}    ${studentGpa13}
#     เลือกDropdown    ${Label_Status}    ${Status_value}
#     กรอกข้อมูล    ${creditPassfiled}    ${creditPass}
#     กรอกข้อมูล    ${creditDownfiled}    ${creditDown}
#     กดปุ่ม    ${btnOk}
    
# TC-PSF-04-07-14ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลหน่วยกิตที่ผ่าน
#     เปิด Browser    ${browser}
#     กรอกข้อมูล    ${studentcodefiled}    ${studentcode}
#     กรอกข้อมูล    ${namestudentfiled}    ${namestudent}
#     เลือกDropdown    ${Label_Level}    ${Lavel_value}
#     กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}
#     เลือกDropdown    ${Label_Status}    ${Status_value}
#     กรอกข้อมูล    ${creditPassfiled}    ${creditPass14}
#     กรอกข้อมูล    ${creditDownfiled}    ${creditDown}
#     กดปุ่ม    ${btnOk}

TC-PSF-04-07-15ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลหน่วยกิตที่ลง
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${studentcodefiled}    ${studentcode}
    กรอกข้อมูล    ${namestudentfiled}    ${namestudent}
    เลือกDropdown    ${Label_Level}    ${Lavel_value}
    กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}
    เลือกDropdown    ${Label_Status}    ${Status_value}
    กรอกข้อมูล    ${creditPassfiled}    ${creditPass}
    กรอกข้อมูล    ${creditDownfiled}    ${creditDown15}
    กดปุ่ม    ${btnOk}
