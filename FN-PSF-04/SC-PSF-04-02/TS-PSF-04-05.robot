*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentEnp_add
${studentcodefiled}  studentIDEnp
${studentcode}  66160000
${creditDownfiled}  creditDownEnp
${creditDown}  20
${namestudentfiled}  nameStuEnp
${namestudent}  สมชาย ใจใหญ่
${studentGpafiled}  stuGpaEnp
${studentGpa}  3.03
${creditPassfiled}  creditPassedEnp
${creditPass}  17
${Label_Level}  stuLevelEnp
${Lavel_value}  ตรี พิเศษ
${Label_Status}  stuStatusEnp
${Status_value}  10
${btnOk}  ok
${btnCancle}  cancel


*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}

เลือกDropdown
    [Arguments]  ${label_name}  ${value}
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Select From List By Label    ${label_name}  ${value}

*** Test Cases ***
TC-PSF-04-05-01ตรวจสอบการกดปุ่มบันทึก
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${studentcodefiled}    ${studentcode}
    กรอกข้อมูล    ${namestudentfiled}    ${namestudent}
    เลือกDropdown    ${Label_Level}    ${Lavel_value}
    กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}
    เลือกDropdown    ${Label_Status}    ${Status_value}
    กรอกข้อมูล    ${creditPassfiled}    ${creditPass}
    กรอกข้อมูล    ${creditDownfiled}    ${creditDown}
    กดปุ่ม    ${btnOk}

TC-PSF-04-05-02ตรวจสอบการกดปุ่มยกเลิก
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${studentcodefiled}    ${studentcode}
    กรอกข้อมูล    ${namestudentfiled}    ${namestudent}
    เลือกDropdown    ${Label_Level}    ${Lavel_value}
    กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}
    เลือกDropdown    ${Label_Status}    ${Status_value}
    กรอกข้อมูล    ${creditPassfiled}    ${creditPass}
    กรอกข้อมูล    ${creditDownfiled}    ${creditDown}
    กดปุ่ม    ${btnCancle}