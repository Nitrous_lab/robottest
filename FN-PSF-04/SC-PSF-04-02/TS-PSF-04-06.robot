*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentEnp_add
${Label_Level}  stuLevelEnp
${Label_Status}  stuStatusEnp

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

คลิกDropdown
    [Arguments]  ${label_name} 
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Click Element    ${label_name}
*** Test Cases ***
TC-PSF-04-06-01ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ระดับ
    เปิด Browser    ${browser}
    คลิกDropdown    ${Label_Level}

TC-PSF-04-06-02ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown สถานภาพ
    เปิด Browser    ${browser}
    คลิกDropdown    ${Label_Status}
