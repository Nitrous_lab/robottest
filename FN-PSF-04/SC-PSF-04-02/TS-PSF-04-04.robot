*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentEnp_add
${studentcodefiled}  studentIDEnp
${studentcode}  66160000
${studentcode06}  123456789
${creditDownfiled}  creditDownEnp
${creditDown}  20
${creditDown07}  1234
${namestudentfiled}  nameStuEnp
${namestudent}  สมชาย ใจใหญ่
${namestudent08}  111111111122222222223333333333444444444555555556
${studentGpafiled}  stuGpaEnp
${studentGpa}  3.03
${studentGpa09}  3.123
${creditPassfiled}  creditPassedEnp
${creditPass}  17
${creditPass10}  1234
${Label_Level}  stuLevelEnp
${Lavel_value}  ตรี พิเศษ
${Label_Status}  stuStatusEnp
${Status_value}  10
${btnOk}  ok


*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}

เลือกDropdown
    [Arguments]  ${label_name}  ${value}
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Select From List By Label    ${label_name}  ${value}

*** Test Cases ***
TC-PSF-04-04-01ตรวจสอบการกรอกข้อมูลรหัสนิสิต
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${studentcodefiled}    ${studentcode}

TC-PSF-04-04-02ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลง
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${creditDownfiled}    ${creditDown}

TC-PSF-04-04-03ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${namestudentfiled}    ${namestudent}

TC-PSF-04-04-04ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ย
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}

TC-PSF-04-04-05ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่าน
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${creditPassfiled}    ${creditPass}

TC-PSF-04-04-06ตรวจสอบการกรอกข้อมูลรหัสนิสิต รับไม่เกิน 8 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${studentcodefiled}    ${studentcode06}

TC-PSF-04-04-07ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลง รับเลขไม่เกิน 3 หลัก
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${creditDownfiled}    ${creditDown07}


TC-PSF-04-04-08ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล รับไม่เกิน 50 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${namestudentfiled}    ${namestudent08}


TC-PSF-04-04-09ตรวจสอบการกรอกข้อมูล เกรดเฉลี่ย รับเลขทศนิยม
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${studentGpafiled}    ${studentGpa09}

TC-PSF-04-04-10ตรวจสอบการกรอกข้อมูล หน่วยกิตที่ผ่าน รับเลขไม่เกิน 3 หลัก
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${creditPassfiled}    ${creditPass10}

TC-PSF-04-04-11ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลรหัสนิสิต
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${namestudentfiled}    ${namestudent}
    เลือกDropdown    ${Label_Level}    ${Lavel_value}
    กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}
    เลือกDropdown    ${Label_Status}    ${Status_value}
    กรอกข้อมูล    ${creditPassfiled}    ${creditPass}
    กรอกข้อมูล    ${creditDownfiled}    ${creditDown}
    กดปุ่ม    ${btnOk}


TC-PSF-04-04-12ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูล หน่วยกิตที่ลง
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${studentcodefiled}    ${studentcode}
    กรอกข้อมูล    ${namestudentfiled}    ${namestudent}
    เลือกDropdown    ${Label_Level}    ${Lavel_value}
    กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}
    เลือกDropdown    ${Label_Status}    ${Status_value}
    กรอกข้อมูล    ${creditPassfiled}    ${creditPass}
    กดปุ่ม    ${btnOk}

TC-PSF-04-04-13ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูล ชื่อ-นามสกุล
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${studentcodefiled}    ${studentcode}
    เลือกDropdown    ${Label_Level}    ${Lavel_value}
    กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}
    เลือกDropdown    ${Label_Status}    ${Status_value}
    กรอกข้อมูล    ${creditPassfiled}    ${creditPass}
    กรอกข้อมูล    ${creditDownfiled}    ${creditDown}
    กดปุ่ม    ${btnOk}


TC-PSF-04-04-14ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูล เกรดเฉลี่ย
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${studentcodefiled}    ${studentcode}
    กรอกข้อมูล    ${namestudentfiled}    ${namestudent}
    เลือกDropdown    ${Label_Level}    ${Lavel_value}
    เลือกDropdown    ${Label_Status}    ${Status_value}
    กรอกข้อมูล    ${creditPassfiled}    ${creditPass}
    กรอกข้อมูล    ${creditDownfiled}    ${creditDown}
    กดปุ่ม    ${btnOk}


TC-PSF-04-04-15ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูล หน่วยกิตที่ผ่าน
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${studentcodefiled}    ${studentcode}
    กรอกข้อมูล    ${namestudentfiled}    ${namestudent}
    เลือกDropdown    ${Label_Level}    ${Lavel_value}
    กรอกข้อมูล    ${studentGpafiled}    ${studentGpa}
    เลือกDropdown    ${Label_Status}    ${Status_value}
    กรอกข้อมูล    ${creditDownfiled}    ${creditDown}
    กดปุ่ม    ${btnOk}

















