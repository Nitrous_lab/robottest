*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://127.0.0.1:8080/manageUser
${input_search}  search
${SearchBtn}  btnsearch
${AddBtn}  btnAdd
${EditBtn}  /manageUser_edit/1
${DelBtn}  /delUser/1

*** Keywords ***
เปิด Browser   
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange}

*** Test Cases ***
TC-PSF-02-02-01ตรวจสอบการกดปุ่มค้นหา
    เปิด Browser   ${browser}
    กดปุ่ม    ${SearchBtn}
TC-PSF-02-02-02ตรวจสอบการกดปุ่มเพิ่มข้อมูล
    เปิด Browser    ${browser}
    กดปุ่ม    ${AddBtn}

TC-PSF-02-02-03ตรวจสอบการกดปุ่มแก้ไขข้อมูล
    เปิด Browser    ${browser}
    กดลิ้งค์    ${EditBtn}
TC-PSF-02-02-04ตรวจสอบการกดปุ่มลบข้อมูล
    เปิด Browser    ${browser}
    กดลิ้งค์    ${DelBtn}