*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://127.0.0.1:8080/
${input_user_name}  email
${user_name01}  pichet
${input_password}  password
${pasword01}  123456
${loginBtn}  btnok
${user_name02}  somnuk
${pasword02}  123456

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

*** Test Cases ***
TC-PSF-02-03-01ตรวจสอบการแสดงผลข้อมูลรายชื่อผู้ใช้งานสถานะเจ้าหน้าที่
    เปิด Browser     ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name01}
    กรอกข้อมูล    ${input_password}    ${pasword01}
    กดปุ่ม    ${loginBtn}

TC-PSF-02-03-02ตรวจสอบการแสดงผลข้อมูลรายชื่อผู้ใช้งานสถานะอาจารย์
    เปิด Browser     ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name02}
    กรอกข้อมูล    ${input_password}    ${pasword02}
    กดปุ่ม    ${loginBtn}
    