*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://127.0.0.1:8080/manageUser
${input_search}  search
${search}  pichet
${search02}  พิเชษ
${search03}  วะยะลุน
${search04}  55555555555555555555555555555555555555555555555555
${SearchBtn}  btnsearch
*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser  
    Open Browser  ${URL}  ${browser}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}
*** Test Cases ***
TC-PSF-02-01-01ตรวจสอบการกรอกข้อมูลค้นหาจากชื่อผู้ใช้
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_search}    ${search}
    กดปุ่ม    ${SearchBtn}

TC-PSF-02-01-02ตรวจสอบการกรอกข้อมูลค้นหาจากชื่อ
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_search}    ${search}
    กดปุ่ม    ${SearchBtn}

TC-PSF-02-01-03ตรวจสอบการกรอกข้อมูลค้นหาจากนามสกุล
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_search}    ${search}
    กดปุ่ม    ${SearchBtn}

TC-PSF-02-01-04ตรวจสอบการกรอกข้อมูลค้นหา รับไม่เกิน 50 ตัวอักษร
    เปิด Browser  ${browser}
    กรอกข้อมูล    ${input_search}    ${search}
    กดปุ่ม    ${SearchBtn}