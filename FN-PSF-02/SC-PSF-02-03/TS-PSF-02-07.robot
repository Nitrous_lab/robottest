*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://127.0.0.1:8080/manageUser
${select_label}  statusUser
${label}  อาจารย์
${input_user_name}  username
${user_name}  nonny
${user_name04}  nonny69
${input_password}  password
${password}  12345
${input_name}  nameuser
${name}  จัตตรงณ์ เพิ่มศิลป์
${password05}  123
${name06}  5555555555555555555555555555555555555555555555555555
${select_label}  statusUser
${label}  เจ้าหน้าที่
${Savebtn}  ok
${Editlink}  /manageUser_edit/1
${user_name07}
${password08}
${name09}

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

เลือกDropdown
    [Arguments]  ${label_name}  ${value}
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Select From List By Label    ${label_name}  ${value}
กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange}    
*** Test Cases ***
TC-PSF-02-07-01ตรวจสอบการกรอกข้อมูลชื่อผู้ใช้
    เปิด Browser  ${browser}
    กดลิ้งค์    ${Editlink}
    กรอกข้อมูล    ${input_user_name}    ${user_name}

TC-PSF-02-07-02ตรวจสอบการกรอกข้อมูลรหัสผ่าน
    เปิด Browser  ${browser}
    กดลิ้งค์    ${Editlink}
    กรอกข้อมูล    ${input_password}    ${password}

TC-PSF-02-07-03ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล
    เปิด Browser  ${browser}
    กดลิ้งค์    ${Editlink}
    กรอกข้อมูล    ${input_name}    ${name}

TC-PSF-02-07-04ตรวจสอบการกรอกข้อมูลชื่อผู้ใช้รับไม่เกิน 6 ตัวอักษร และไม่น้อยกว่า 6 ตัวอักษร
    เปิด Browser  ${browser}
    กดลิ้งค์    ${Editlink}
    กรอกข้อมูล    ${input_user_name}    ${user_name04}

TC-PSF-02-07-05ตรวจสอบการกรอกข้อมูลรหัสผ่านรับไม่เกิน 6 ตัวอักษร และไม่น้อยกว่า 6 ตัวอักษร
    เปิด Browser  ${browser}
    กดลิ้งค์    ${Editlink}
    กรอกข้อมูล    ${input_password}    ${password05}

TC-PSF-02-07-06ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุลรับไม่เกิน 50 ตัวอักษร
    เปิด Browser  ${browser}
    กดลิ้งค์    ${Editlink}
    กรอกข้อมูล    ${input_name}    ${name06}

TC-PSF-02-07-07ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลชื่อผู้ใช้
    เปิด Browser  ${browser}
    กดลิ้งค์    ${Editlink}
    กรอกข้อมูล    ${input_user_name}    ${user_name07}
    กรอกข้อมูล    ${input_password}    ${password}
    กรอกข้อมูล    ${input_name}    ${name}
    เลือกDropdown    ${select_label}  ${label}
    กดปุ่ม  ${Savebtn}

TC-PSF-02-07-08ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลรหัสผ่าน
    เปิด Browser  ${browser}
    กดลิ้งค์    ${Editlink}
    กรอกข้อมูล    ${input_user_name}    ${user_name}
    กรอกข้อมูล    ${input_password}    ${password08}
    กรอกข้อมูล    ${input_name}    ${name}
    เลือกDropdown    ${select_label}  ${label}
    กดปุ่ม  ${Savebtn}

TC-PSF-02-07-09ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลชื่อ-นามสกุล
    เปิด Browser  ${browser}
    กดลิ้งค์    ${Editlink}
    กรอกข้อมูล    ${input_user_name}    ${user_name}
    กรอกข้อมูล    ${input_password}    ${password}
    กรอกข้อมูล    ${input_name}    ${name09}
    เลือกDropdown    ${select_label}  ${label}
    กดปุ่ม  ${Savebtn}













