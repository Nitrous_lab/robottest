*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://127.0.0.1:8080/manageUser
${input_name}  nameuser
${Savebtn}  ok
${Cancelbtn}  cancel
${Editlink}  /manageUser_edit/1


*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}
กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange}  
 




*** Test Cases ***
TC-PSF-02-08-01ตรวจสอบการกดปุ่มบันทึก
    เปิด Browser    ${browser}
    กดลิ้งค์    ${Editlink}
    กดปุ่ม  ${Savebtn}

TC-PSF-02-08-02ตรวจสอบการกดปุ่มยกเลิก
    เปิด Browser   ${browser}
    กดลิ้งค์    ${Editlink}
    กดปุ่ม  ${Cancelbtn}