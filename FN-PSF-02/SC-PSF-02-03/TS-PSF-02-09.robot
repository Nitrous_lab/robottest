*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://127.0.0.1:8080/manageUser
${Link}  /manageUser_edit/1
${Select}  statusUser

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}
กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 
คลิกDropdown
    [Arguments]  ${label_name} 
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Click Element    ${label_name}



*** Test Cases ***
TC-PSF-02-09-01ตรวจสอบการแสดงผลข้อมูลผู้ใช้ที่เลือกจากตาราง
    เปิด Browser    ${browser}
    กดลิ้งค์    ${Link}

TC-PSF-02-09-02ตรวจสอบการแสดงผลของฟิลเตอร์ Dropdown ข้อมูลตำแหน่ง
    เปิด Browser    ${browser}
    กดลิ้งค์    ${Link}
    คลิกDropdown    ${Select}