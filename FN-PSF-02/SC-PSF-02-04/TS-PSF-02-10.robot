*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://127.0.0.1:8080/manageUser
${Link}  /delUser/27
${Link02}  /delUser/1
${Okbtn}  ok
${Cancelbtn}  cancel

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}
กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 




*** Test Cases ***
TC-PSF-02-10-01ตรวจสอบการกดปุ่มตกลง
    เปิด Browser    ${browser}
    กดลิ้งค์    ${Link}
    กดปุ่ม  ${Okbtn}

TC-PSF-02-10-02ตรวจสอบการกดปุ่มยกเลิก
    เปิด Browser    ${browser}
    กดลิ้งค์    ${Link02}
    กดปุ่ม  ${Cancelbtn}

