*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://127.0.0.1:8080/manageUser_add
${select_label}  statusUser
${label}  อาจารย์

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}    ${browser}

คลิกDropdown
    [Arguments]  ${label_name} 
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Click Element    ${label_name}



*** Test Cases ***
TC-PSF-02-06-01ตรวจสอบการแสดงผลของฟิลเตอร์ Dropdown ข้อมูลตำแหน่ง
    เปิด Browser    ${browser}
    คลิกDropdown    ${select_label}
