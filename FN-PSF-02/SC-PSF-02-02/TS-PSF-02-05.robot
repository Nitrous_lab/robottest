*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://127.0.0.1:8080/manageUser_add
${input_user_name}  username
${user_name}  nonny
${input_password}  password
${password}  12345
${input_name}  nameuser
${name}  จัตตรงณ์ เพิ่มศิลป์
${select_label}  statusUser
${label}  เจ้าหน้าที่
${Savebtn}  ok
${Cancelbtn}  cancel

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}    ${browser}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

เลือกDropdown
    [Arguments]  ${label_name}  ${value}
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Select From List By Label    ${label_name}  ${value}


*** Test Cases ***
TC-PSF-02-05-01ตรวจสอบการกดปุ่มบันทึก
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name}
    กรอกข้อมูล    ${input_name}    ${name}
    กรอกข้อมูล    ${input_password}    ${password}
    เลือกDropdown    ${select_label}  ${label}
    กดปุ่ม  ${Savebtn}

TC-PSF-02-05-02ตรวจสอบการกดปุ่มบันทึก
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name}
    กรอกข้อมูล    ${input_name}    ${name}
    กรอกข้อมูล    ${input_password}    ${password}
    เลือกDropdown    ${select_label}  ${label}
    กดปุ่ม  ${Savebtn}