*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://127.0.0.1:8080/manageUser_add
${input_user_name}  username
${user_name}  nonny
${input_password}  password
${password}  12345
${input_name}  nameuser
${name}  จัตตรงณ์ เพิ่มศิลป์
${user_name04}  nonny69
${password05}  123
${name06}  5555555555555555555555555555555555555555555555555555
${name07}  จัตตรงณ์ เพิ่มศิลป์
${password07}  12345
${select_label}  statusUser
${label}  เจ้าหน้าที่
${Savebtn}  ok

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

เลือกDropdown
    [Arguments]  ${label_name}  ${value}
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Select From List By Label    ${label_name}  ${value}




*** Test Cases ***
TC-PSF-02-04-01ตรวจสอบการกรอกข้อมูลชื่อผู้ใช้
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name}

TC-PSF-02-04-02ตรวจสอบการกรอกข้อมูลรหัสผ่าน
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_password}    ${password}

TC-PSF-02-04-03ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_name}    ${name}

TC-PSF-02-04-04ตรวจสอบการกรอกข้อมูลชื่อผู้ใช้รับไม่เกิน 6 ตัวอักษร และไม่น้อยกว่า 6 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name04}

TC-PSF-02-04-05ตรวจสอบการกรอกข้อมูลรหัสผ่านรับไม่เกิน 6 ตัวอักษร และไม่น้อยกว่า 6 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_password}    ${password05}

TC-PSF-02-04-06ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุลรับไม่เกิน 50 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_name}    ${name06}

TC-PSF-02-04-07ตรวจสอบการกรอกข้อมูลหากไม่กรอกข้อมูลชื่อผู้ใช้
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_name}    ${name07}
    กรอกข้อมูล    ${input_password}    ${password07}
    เลือกDropdown    ${select_label}  ${label}
    กดปุ่ม  ${Savebtn}

TC-PSF-02-04-08ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลรหัสผ่าน
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name}
    กรอกข้อมูล    ${input_name}    ${name07}
    เลือกDropdown    ${select_label}  ${label}
    กดปุ่ม  ${Savebtn}

TC-PSF-02-04-09ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลชื่อ-นามสกุล
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name}
    กรอกข้อมูล    ${input_password}    ${password07}
    เลือกDropdown    ${select_label}  ${label}
    กดปุ่ม  ${Savebtn}
