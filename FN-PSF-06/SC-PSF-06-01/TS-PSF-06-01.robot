*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/followStudentEnp
${searchfiled}  search
${search_input01}  58660044
${search_input02}  ชรัญธร
${search_input03}  วงษ์ไทย
${search_input04}  ชรัญธร วงษ์ไทย
${search_input05}  58660044 ชรัญธร วงษ์ไทย
${search_input07}  55555555555555555555555555555555555555555555555555
${search_input06}  
${btnSearch}  btnsearch

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}

*** Test Cases ***
TC-PSF-06-01-01ตรวจสอบการกรอกข้อมูลค้นหาจากรหัสนิสิต
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input01}
    กดปุ่ม    ${btnSearch}

TC-PSF-06-01-02ตรวจสอบการกรอกข้อมูลค้นหาจากชื่อ
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input02}
    กดปุ่ม    ${btnSearch}

TC-PSF-06-01-03ตรวจสอบการกรอกข้อมูลค้นหาจากนามสกุล
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input03}
    กดปุ่ม    ${btnSearch}

TC-PSF-06-01-04ตรวจสอบการกรอกข้อมูลค้นหาจากการใส่ชื่อ-นามสกุล
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input04}
    กดปุ่ม    ${btnSearch}

TC-PSF-06-01-05ตรวจสอบการกรอกข้อมูลค้นหาจากการใส่ชื่อ-นามสกุล รหัสนิสิต
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input05}
    กดปุ่ม    ${btnSearch}

TC-PSF-06-01-06ตรวจสอบการกรอกข้อมูลโดยไม่กรอกอะไรเลย
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input06}
    กดปุ่ม    ${btnSearch}

TC-PSF-06-01-07ตรวจสอบการกรอกข้อมูลค้นหาโดยรับตัวอักษรเกิน 50 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input07}
    กดปุ่ม    ${btnSearch}