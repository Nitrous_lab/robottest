*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/followStudentEnp
${searchfiled}  search
${search_input01}  58660044
${btnSearch}  btnsearch
${followLink}  /followStudentEnpReg/13

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 

*** Test Cases ***
TC-PSF-06-02-01ตรวจสอบการกดปุ่มค้นหา
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input01}
    กดปุ่ม    ${btnSearch}

TC-PSF-06-02-02ตรวจสอบการกดปุ่มไอคอนติดตาม
    เปิด Browser    ${browser}
    กดลิ้งค์    ${followLink}
