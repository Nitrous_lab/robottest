*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://127.0.0.1:8080/
${input_user_name}  email
${user_name01}  pichet
${user_name03}  somnuk
${pasword01}  123456
${input_password}  password
${loginBtn}  btnok

*** Keywords ***
เปิด Browser  
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser
    Open Browser  ${URL}  ${browser}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

*** Test Cases ***
TC-PSF-01-03-01 ตรวจสอบการแสดงผลชื่อผู้ใช้งาน
    เปิด Browser  ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name01}
    กรอกข้อมูล    ${input_password}    ${pasword01}
    กดปุ่ม    ${loginBtn}

TC-PSF-01-03-02 ตรวจสอบการแสดงผลสถานะผู้ใช้งานสถานะอาจารย์
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name01}
    กรอกข้อมูล    ${input_password}    ${pasword01}
    กดปุ่ม    ${loginBtn}

TC-PSF-01-03-03 ตรวจสอบการแสดงผลสถานะผู้ใช้งานสถานะเจ้าหน้าที่
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name03}
    กรอกข้อมูล    ${input_password}    ${pasword01}
    กดปุ่ม    ${loginBtn}

TC-PSF-01-03-04 ตรวจสอบการแสดงผลข้อมูลรายชื่อนิสิตรอการพินิจในระบบ
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name03}
    กรอกข้อมูล    ${input_password}    ${pasword01}
    กดปุ่ม    ${loginBtn}