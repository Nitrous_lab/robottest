*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://127.0.0.1:8080/
${input_user_name}  email
${user_name}  somnuk
${pasword}  123456
${input_password}  password
${loginBtn}  btnok

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser
    Open Browser  ${URL}  ${browser}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

*** Test Cases ***
TC-PSF-01-02-01 ตรวจสอบการกดปุ่มเข้าสู่ระบบ
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name}
    กรอกข้อมูล    ${input_password}    ${pasword}
    กดปุ่ม    ${loginBtn}