*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://127.0.0.1:8080/
${input_user_name}  email
${user_name01}  pichet
${user_name02}  pichet01
${user_name03}  piche
${user_name09}  somnuk
${input_password}  password
${pasword04}  123456
${pasword05}  1234567
${pasword06}  12345
${loginBtn}  btnok
${user_name09}  suda

*** Keywords ***
เปิด Browser  
    [Arguments]    ${browser}
    [Documentation]  กำหนด Web Browser
    Open Browser  ${URL}  ${browser}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

*** Test Cases ***
TC-PSF-01-01-01 ตรวจสอบการกรอกข้อมูลUsername รับไม่เกินจำนวน 6 ตัวอักษรและไม่น้อยกว่า 6 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล  ${input_user_name}    ${user_name01}

TC-PSF-01-01-02 ตรวจสอบการกรอกข้อมูลUsername รับเกินจำนวน 6 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name02}

TC-PSF-01-01-03 ตรวจสอบการกรอกข้อมูลUsername รับน้อยกว่าจำนวน 6 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name03}

TC-PSF-01-01-04 ตรวจสอบการกรอกข้อมูลPassword รับไม่เกินจำนวน 6 ตัวอักษรและไม่น้อยกว่า 6 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_password}    ${pasword04}

TC-PSF-01-01-05 ตรวจสอบการกรอกข้อมูลPassword รับเกินจำนวน 6 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_password}    ${pasword05}

TC-PSF-01-01-06 ตรวจสอบการกรอกข้อมูลPassword รับน้อยกว่าจำนวน 6 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_password}    ${pasword06}

TC-PSF-01-01-07 ตรวจสอบการกรอกข้อมูลPassword รับข้อตวามที่กรอกไม่แสดงตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล  ${input_password}    ${pasword06}

TC-PSF-01-01-08 ตรวจสอบการเข้าสู่ระบบสำหรับผู้ใช้สถานะอาจารย์
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name01}
    กรอกข้อมูล    ${input_password}    ${pasword04}
    กดปุ่ม    ${loginBtn}

TC-PSF-01-01-09 ตรวจสอบการเข้าสู่ระบบสำหรับผู้ใช้สถานะเจ้าหน้าที่
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name09}
    กรอกข้อมูล    ${input_password}    ${pasword04}
    กดปุ่ม    ${loginBtn}

TC-PSF-01-01-10 ตรวจสอบการเข้าสู่ระบบสำหรับผู้ใช้ที่ไม่มีสิทธิเข้าสู่ระบบ
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${input_user_name}    ${user_name09}
    กรอกข้อมูล    ${input_password}    ${pasword04}
    กดปุ่ม    ${loginBtn}