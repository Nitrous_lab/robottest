*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentConsider
${Label_level}  stuLevelCon
${Label_status}  stuStatusCon
${Label_consider}  considerCon
${Label_considertime}  considertime
${editLink}  /manageCon_edit/15

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

คลิกDropdown
    [Arguments]  ${label_name} 
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Click Element    ${label_name}

กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 

*** Test Cases ***
TC-PSF-03-09-01ตรวจสอบการแสดงผลของฟิลเตอร์ Dropdown ข้อมูลระดับ
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    คลิกDropdown    ${Label_level}
TC-PSF-03-09-02ตรวจสอบการแสดงผลของฟิลเตอร์ Dropdown ข้อมูลสถานภาพ
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    คลิกDropdown    ${Label_status}

TC-PSF-03-09-03ตรวจสอบการแสดงผลของฟิลเตอร์ Dropdown ข้อมูลรอการพิจารณา
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    คลิกDropdown    ${Label_consider}

TC-PSF-03-09-04ตรวจสอบการแสดงผลของฟิลเตอร์ Dropdown ข้อมูลครั้งที่
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    คลิกDropdown    ${Label_considertime}

TC-PSF-03-09-05ตรวจสอบการแสดงผลข้อมูลผู้ใช้ที่เลือกจากตาราง
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}





    