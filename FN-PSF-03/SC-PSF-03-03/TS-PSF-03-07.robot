*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentConsider
${Label_level}  stuLevelCon
${Label_status}  stuStatusCon
${Label_consider}  considerCon
${Label_considertime}  considertime
${editLink}  /manageCon_edit/15
${input_studencode}  64160237
${input_studencode11}   
${input_studencode06}  641602375
${studencodefiled}  studentIDCon
${input_name}  จัตตรงณ์ เพิ่มศิลป์
${input_name12}  
${input_name07}  55555555555555555555555555555555555555555555555555
${namefiled}  nameStuCon
${gradefiled}  stuGpaCon
${input_grade}  1.90
${input_grade13}  
${input_grade08}  1.90007
${passgradefiled}  creditPassedCon
${input_passgrade}  70
${input_passgrade14}  
${input_passgrade09}  70000
${Downgradefiled}  creditDownCon
${input_Downgrade}  95
${input_Downgrade15}  
${input_Downgrade10}  95000
${Label_level}  stuLevelCon
${Label_status}  stuStatusCon
${Label_consider}  considerCon
${Label_considertime}  considertime
${btnOK}  ok
${consider_value}  โปรต่ำ
${considertime_value}  1
${Level_value}  ตรี พิเศษ
${Status_value}  10








*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}

กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 

เลือกDropdown
    [Arguments]  ${label_name}  ${value}
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Select From List By Label    ${label_name}  ${value}

กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

*** Test Cases ***
TC-PSF-03-07-01ตรวจสอบการกรอกข้อมูลรหัสนิสิต
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${studencodefiled}    ${input_studencode}
    
TC-PSF-03-07-02ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${namefiled}    ${input_name}

TC-PSF-03-07-03ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ย
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${gradefiled}    ${input_grade}

TC-PSF-03-07-04ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่าน
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${passgradefiled}    ${input_passgrade}

TC-PSF-03-07-05ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลง
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${Downgradefiled}    ${input_Downgrade}

TC-PSF-03-07-06ตรวจสอบการกรอกข้อมูลรหัสนิสิต รับไม่เกิน 8 ตัวอักษร และ รับไม่น้อยกว่า 8 ตัวอักษร
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${studencodefiled}    ${input_studencode06}

TC-PSF-03-07-07ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล รับไม่เกิน 50 ตัวอักษร
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${namefiled}    ${input_name07}

TC-PSF-03-07-08ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ยรับทศนิยมไม่เกิน 2 หลัก และรับไม่น้อยกว่า 2 หลัก
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${gradefiled}    ${input_grade08}

TC-PSF-03-07-09ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่านรับเลขไม่เกิน 3 หลัก
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${passgradefiled}    ${input_passgrade09}

TC-PSF-03-07-10ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลงรับเลขไม่เกิน 3 หลัก
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${Downgradefiled}    ${input_Downgrade10}

TC-PSF-03-07-11ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลรหัสนิสิต
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${studencodefiled}    ${input_studencode11}
    กรอกข้อมูล    ${namefiled}    ${input_name}
    กรอกข้อมูล    ${gradefiled}    ${input_grade}
    เลือกDropdown    ${Label_level}    ${Level_value}
    เลือกDropdown    ${Label_status}    ${Status_value}
    กรอกข้อมูล    ${Downgradefiled}    ${input_Downgrade}
    กรอกข้อมูล    ${passgradefiled}    ${input_passgrade}
    เลือกDropdown    ${Label_consider}    ${consider_value}
    เลือกDropdown    ${Label_considertime}    ${considertime_value}
    กดปุ่ม    ${btnOK}

TC-PSF-03-07-12ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลชื่อ-นามสกุล
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${studencodefiled}    ${input_studencode}
    กรอกข้อมูล    ${namefiled}    ${input_name12}
    กรอกข้อมูล    ${gradefiled}    ${input_grade}
    เลือกDropdown    ${Label_level}    ${Level_value}
    เลือกDropdown    ${Label_status}    ${Status_value}
    กรอกข้อมูล    ${Downgradefiled}    ${input_Downgrade}
    กรอกข้อมูล    ${passgradefiled}    ${input_passgrade}
    เลือกDropdown    ${Label_consider}    ${consider_value}
    เลือกDropdown    ${Label_considertime}    ${considertime_value}
    กดปุ่ม    ${btnOK}

TC-PSF-03-07-13ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลเกรดเฉลี่ย
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${studencodefiled}    ${input_studencode}
    กรอกข้อมูล    ${namefiled}    ${input_name}
    กรอกข้อมูล    ${gradefiled}    ${input_grade13}
    เลือกDropdown    ${Label_level}    ${Level_value}
    เลือกDropdown    ${Label_status}    ${Status_value}
    กรอกข้อมูล    ${Downgradefiled}    ${input_Downgrade}
    กรอกข้อมูล    ${passgradefiled}    ${input_passgrade}
    เลือกDropdown    ${Label_consider}    ${consider_value}
    เลือกDropdown    ${Label_considertime}    ${considertime_value}
    กดปุ่ม    ${btnOK}

TC-PSF-03-07-14ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลหน่วยกิตที่ผ่าน
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${studencodefiled}    ${input_studencode}
    กรอกข้อมูล    ${namefiled}    ${input_name}
    กรอกข้อมูล    ${gradefiled}    ${input_grade}
    เลือกDropdown    ${Label_level}    ${Level_value}
    เลือกDropdown    ${Label_status}    ${Status_value}
    กรอกข้อมูล    ${Downgradefiled}    ${input_Downgrade}
    กรอกข้อมูล    ${passgradefiled}    ${input_passgrade14}
    เลือกDropdown    ${Label_consider}    ${consider_value}
    เลือกDropdown    ${Label_considertime}    ${considertime_value}
    กดปุ่ม    ${btnOK}

TC-PSF-03-07-15ตรวจสอบการกรอกข้อมูล หากไม่กรอกข้อมูลหน่วยกิตที่ลง
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}
    กรอกข้อมูล    ${studencodefiled}    ${input_studencode}
    กรอกข้อมูล    ${namefiled}    ${input_name}
    กรอกข้อมูล    ${gradefiled}    ${input_grade}
    เลือกDropdown    ${Label_level}    ${Level_value}
    เลือกDropdown    ${Label_status}    ${Status_value}
    กรอกข้อมูล    ${Downgradefiled}    ${input_Downgrade15}
    กรอกข้อมูล    ${passgradefiled}    ${input_passgrade}
    เลือกDropdown    ${Label_consider}    ${consider_value}
    เลือกDropdown    ${Label_considertime}    ${considertime_value}
    กดปุ่ม    ${btnOK}














    
    

