*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentCon_add
${Label_level}  stuLevelCon
${Label_status}  stuStatusCon
${Label_consider}  considerCon
${Label_considertime}  considertime
*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

คลิกDropdown
    [Arguments]  ${label_name} 
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Click Element    ${label_name}

*** Test Cases ***
TC-PSF-03-06-01ตรวจสอบการแสดงผลของฟิลเตอร์ Dropdown ข้อมูลระดับ
    เปิด Browser    ${browser}
    คลิกDropdown    ${Label_level}

TC-PSF-03-06-02ตรวจสอบการแสดงผลของฟิลเตอร์ Dropdown ข้อมูลสถานภาพ
    เปิด Browser    ${browser}
    คลิกDropdown    ${Label_status}

TC-PSF-03-06-03ตรวจสอบการแสดงผลของฟิลเตอร์ Dropdown ข้อมูลรอการพิจารณา
    เปิด Browser    ${browser}
    คลิกDropdown    ${Label_consider}

TC-PSF-03-06-04ตรวจสอบการแสดงผลของฟิลเตอร์ Dropdown ข้อมูลครั้งที่
    เปิด Browser    ${browser}
    คลิกDropdown    ${Label_considertime}



