*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentCon_add
${studencodefiled}  studentIDCon
${namefiled}  nameStuCon
${gradefiled}  stuGpaCon
${passgradefiled}  creditPassedCon
${Downgradefiled}  creditDownCon
${input_studencode}  64160237
${input_name}  จัตตรงณ์ เพิ่มศิลป์
${input_grade}  1.90
${input_passgrade}  70
${input_Downgrade}  95
${Label_level}  stuLevelCon
${Label_status}  stuStatusCon
${Label_consider}  considerCon
${Label_considertime}  considertime
${Level_value}  ตรี พิเศษ
${Status_value}  10
${consider_value}  โปรต่ำ
${considertime_value}  1
${btnOK}  ok
${btnCancle}  cancel



*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}
กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
เลือกDropdown
    [Arguments]  ${label_name}  ${value}
    [Documentation]  กำหนดDropdownต้องการคลิก และเลือกค่า
    Select From List By Label    ${label_name}  ${value}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

*** Test Cases ***
TC-PSF-03-05-01ตรวจสอบการกดปุ่มบันทึก
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${studencodefiled}    ${input_studencode}
    กรอกข้อมูล    ${namefiled}    ${input_name}
    กรอกข้อมูล    ${gradefiled}    ${input_grade}
    เลือกDropdown    ${Label_level}    ${Level_value}
    เลือกDropdown    ${Label_status}    ${Status_value}
    กรอกข้อมูล    ${Downgradefiled}    ${input_Downgrade}
    กรอกข้อมูล    ${passgradefiled}    ${input_passgrade}
    เลือกDropdown    ${Label_consider}    ${consider_value}
    เลือกDropdown    ${Label_considertime}    ${considertime_value}
    กดปุ่ม    ${btnOK}

TC-PSF-03-05-02ตรวจสอบการกดปุ่มยกเลิก
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${studencodefiled}    ${input_studencode}
    กรอกข้อมูล    ${namefiled}    ${input_name}
    กรอกข้อมูล    ${gradefiled}    ${input_grade}
    เลือกDropdown    ${Label_level}    ${Level_value}
    เลือกDropdown    ${Label_status}    ${Status_value}
    กรอกข้อมูล    ${Downgradefiled}    ${input_Downgrade}
    กรอกข้อมูล    ${passgradefiled}    ${input_passgrade}
    เลือกDropdown    ${Label_consider}    ${consider_value}
    เลือกDropdown    ${Label_considertime}    ${considertime_value}
    กดปุ่ม    ${btnCancle}


