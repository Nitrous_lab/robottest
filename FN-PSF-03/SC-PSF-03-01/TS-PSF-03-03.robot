*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentConsider

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

*** Test Cases ***
TC-PSF-03-03-01ตรวจสอบการแสดงผลข้อมูลรายชื่อนิสิตรอการพินิจ
    เปิด Browser    ${browser}