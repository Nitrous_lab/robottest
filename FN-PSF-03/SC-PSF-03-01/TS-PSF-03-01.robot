*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentConsider
${searchfiled}  search
${input_search}  63160254
${input_search02}  สมพง
${input_search03}  มีมาก
${input_search04}  55555555555555555555555555555555555555555555555555
${btnSearch}  btnsearch

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}



*** Test Cases ***
TC-PSF-03-01-01ตรวจสอบการกรอกข้อมูลค้นหาจากชื่อรหัสนิสิต
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${input_search}
    กดปุ่ม    ${btnSearch}

    
TC-PSF-03-01-02ตรวจสอบการกรอกข้อมูลค้นหาจากชื่อ
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${input_search02}
    กดปุ่ม    ${btnSearch}

TC-PSF-03-01-03ตรวจสอบการกรอกข้อมูลค้นหาจากนามสกุล
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${input_search03}
    กดปุ่ม    ${btnSearch}

TC-PSF-03-01-04ตรวจสอบการกรอกข้อมูลค้นหา รับไม่เกิน 50 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${input_search04}
    กดปุ่ม    ${btnSearch}