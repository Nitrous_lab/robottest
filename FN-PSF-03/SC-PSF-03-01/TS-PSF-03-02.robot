*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/manageStudentConsider
${searchfiled}  search
${input_search01}  63160254
${btnSearch}  btnsearch
${btnAdd}  btnAdd
${editLink}  /manageCon_edit/1
${deleteLink}  /delCon/1

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}

กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 



*** Test Cases ***
TC-PSF-03-02-01ตรวจสอบการกดปุ่มค้นหา
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${input_search01}
    กดปุ่ม    ${btnSearch}

TC-PSF-03-02-02ตรวจสอบการกดปุ่มเพิ่มข้อมูล
    เปิด Browser    ${browser}
    กดปุ่ม    ${btnAdd}

TC-PSF-03-02-03ตรวจสอบการกดปุ่มแก้ไขข้อมูล
    เปิด Browser    ${browser}
    กดลิ้งค์    ${editLink}

TC-PSF-03-02-04ตรวจสอบการกดปุ่มลบข้อมูล
    เปิด Browser    ${browser}
    กดลิ้งค์    ${deleteLink}

    
    