*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/followStudent
${stuConRegfiled}  stuConReg
${stuConReg_input}  111111111122222222223333333333444444444455555555556
${stuConReg_input03}  ลงทะเบียนเรียบร้อย
${btnOk}  ok
${followLink}  /followStudentCon/1

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}
กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 

*** Test Cases ***
TC-PSF-05-04-01ตรวจสอบการกรอกข้อมูลหากไม่มีการกรอกข้อมูลแผนการเรียน
    เปิด Browser    ${browser}
    กดลิ้งค์    ${followLink}
    กดปุ่ม    ${btnOk}
TC-PSF-05-04-02ตรวจสอบการกรอกข้อมูลแผนการเรียน รับไม่เกิน 50 ตัวอักษร
    เปิด Browser    ${browser}
    กดลิ้งค์    ${followLink}
    กรอกข้อมูล    ${stuConRegfiled}    ${stuConReg_input}
    กดปุ่ม    ${btnOk}

TC-PSF-05-04-03ตรวจสอบการกรอกข้อมูลแผนการเรียน
    เปิด Browser    ${browser}
    กดลิ้งค์    ${followLink}
    กรอกข้อมูล    ${stuConRegfiled}    ${stuConReg_input03}
    กดปุ่ม    ${btnOk}
