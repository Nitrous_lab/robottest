*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/followStudent
${stuConRegfiled}  stuConReg
${stuConReg_input}  ลงทะเบียนเรียบร้อย
${btnOk}  ok
${followLink}  /followStudentCon/1
${delLink}  /deleteRegCon/26


*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}
กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 

*** Test Cases ***
TC-PSF-05-05-01ตรวจสอบปุ่มบันทึก
    เปิด Browser    ${browser}
    กดลิ้งค์    ${followLink}
    กรอกข้อมูล    ${stuConRegfiled}    ${stuConReg_input}
    กดปุ่ม    ${btnOk}
TC-PSF-05-05-02ตรวจสอบปุ่มลบข้อมูล
    เปิด Browser    ${browser}
    กดลิ้งค์    ${followLink}
    กดลิ้งค์    ${delLink}