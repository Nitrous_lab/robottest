*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/followStudent
${searchfiled}  search
${search_input01}  สมชาย
${btnSearch}  btnsearch


*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

*** Test Cases ***
TC-PSF-05-03-01ตรวจสอบการแสดงผลข้อมูลรายชื่อนิสิตรอการพินิจ
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input01}
    กดปุ่ม    ${btnSearch}
