*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/followStudent
${searchfiled}  search
${search_input01}  สมชาย
${search_input02}  สีดา
${search_input03}  5956646
${search_input04}  สมชาย สีดา
${search_input05}  5956646 สมชาย สีดา
${search_input06}   
${search_input07}   111111111122222222223333333333444444444455555555556
${btnSearch}  btnsearch

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}

*** Test Cases ***

TC-PSF-05-01-01ตรวจสอบการกรอกข้อมูลค้นหาจากการใส่เพียงชื่อ
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input01}
    กดปุ่ม    ${btnSearch}
TC-PSF-05-01-02ตรวจสอบการกรอกข้อมูลค้นหาจากการใส่เพียงนามสกุล
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input02}
    กดปุ่ม    ${btnSearch}

TC-PSF-05-01-03ตรวจสอบการกรอกข้อมูลค้นหาจากการใส่เพียงรหัสนิสิต
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input03}
    กดปุ่ม    ${btnSearch}

TC-PSF-05-01-04ตรวจสอบการกรอกข้อมูลค้นหาจากการใส่ชื่อ-นามสกุล
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input04}
    กดปุ่ม    ${btnSearch}

TC-PSF-05-01-05ตรวจสอบการกรอกข้อมูลค้นหาจากการใส่ชื่อ-นามสกุล รหัสนิสิต
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input05}
    กดปุ่ม    ${btnSearch}
TC-PSF-05-01-06ตรวจสอบการกรอกข้อมูลโดยไม่กรอกอะไรเลย
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input06}
    กดปุ่ม    ${btnSearch}
TC-PSF-05-01-07ตรวจสอบการกรอกข้อมูลค้นหา รับไม่เกิน 50 ตัวอักษร
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input07}
    กดปุ่ม    ${btnSearch}