*** Settings ***
Library  SeleniumLibrary
Test Teardown

*** Variables ***
${browser}  edge
${URL}  http://localhost:8080/followStudent
${searchfiled}  search
${search_input01}  สมชาย
${btnSearch}  btnsearch
${followLink}  /followStudentCon/1

*** Keywords ***
เปิด Browser 
    [Arguments]  ${browser}
    [Documentation]  กำหนด Web Browser 
    Open Browser  ${URL}  ${browser}

กดปุ่ม
    [Arguments]  ${Btnname}
    [Documentation]  กำหนดปุ่มที่ต้องการคลิก และคลิกปุ่ม
    Click Button    ${Btnname}

กรอกข้อมูล 
    [Arguments]  ${filedname}  ${input}
    [Documentation]  กรอกข้อมูลข้อมูลลงในfiledที่ต้องการ
    Input Text    ${filedname}    ${input}
กดลิ้งค์
    [Arguments]  ${Link_pange}
    [Documentation]  กำหนดลิ้งค์ที่ต้องการคลิก เพื่อคลิก
    Click Link    ${Link_pange} 

*** Test Cases ***
TC-PSF-05-02-01ตรวจสอบปุ่มค้นหา
    เปิด Browser    ${browser}
    กรอกข้อมูล    ${searchfiled}    ${search_input01}
    กดปุ่ม    ${btnSearch}


TC-PSF-05-02-02ตรวจสอบปุ่มติดตาม
    เปิด Browser    ${browser}
    กดลิ้งค์    ${followLink}

